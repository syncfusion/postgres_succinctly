﻿using System.Data.Entity;
using PostgresExample.Entities;

namespace PostgresExample.Classes
{
  public class EFDatabase : DbContext
  {
    public EFDatabase()
      : base("pgconnection") // Name of connection string to look for
    { }

    public DbSet<MyTable> MyTables { get; set; }

  }
}
