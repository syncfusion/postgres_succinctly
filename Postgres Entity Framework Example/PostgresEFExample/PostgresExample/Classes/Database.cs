﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using PostgresExample.Entities;

namespace PostgresExample.Classes
{
  public class Database
  {
    public List<MyTable> GetAll()
    {
      List<MyTable> results;

      using(EFDatabase myDb = new EFDatabase())
      {
        results = myDb.Set<MyTable>().AsQueryable().ToList();
      }

      return results;
    }

    public void AddNew(MyTable entity)
    {
      using (EFDatabase myDb = new EFDatabase())
      {
        myDb.Set<MyTable>().Add(entity);
        myDb.SaveChanges();
      }

    }

    public void DeleteByGid(int gid)
    {
      using(EFDatabase myDb = new EFDatabase())
      {
        MyTable entityToRemove = myDb.Set<MyTable>().FirstOrDefault(x => x.Gid == gid);
        if (entityToRemove == null) return;
        myDb.Set<MyTable>().Remove(entityToRemove);
        myDb.SaveChanges();
      }

    }

    public void Update(MyTable entity)
    {
      using(EFDatabase myDb = new EFDatabase())
      {

        DbEntityEntry entry = myDb.Entry(entity);
        if (entry != null)
        {
          switch (entry.State)
          {
            case EntityState.Detached:
              myDb.Set<MyTable>().Attach(entity);
              myDb.Entry(entity).State = EntityState.Modified;
              break;

            case EntityState.Deleted:
              entry.CurrentValues.SetValues(entity);
              entry.State = EntityState.Modified;
              break;

            default:
              entry.State = EntityState.Modified;
              break;
          }
        }
        else
        {
          myDb.Set<MyTable>().Attach(entity);
          myDb.Entry(entity).State = EntityState.Modified;
        }

        myDb.ChangeTracker.DetectChanges();
        myDb.SaveChanges();

      }

    }

  }
}
