﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PostgresExample.Entities
{
  // Note: I'm using column attributes to enforce lowercase names and set the defualt schema which is 'public'

  [Table("mytable", Schema = "public")]
  public class MyTable
  {
    [Key]
    [Column("gid")]
    public int Gid { get; set; }

    [Column("name")]
    public string Name { get; set; }

    [Column("email")]
    public string Email { get; set; }

    [Column("webpage")]
    public string Webpage { get; set; }

    [Column("bio")]
    public string Bio { get; set; }

  }
}
