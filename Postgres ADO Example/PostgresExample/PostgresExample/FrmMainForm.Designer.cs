﻿namespace PostgresExample
{
  partial class FrmMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabDataViews = new System.Windows.Forms.TabControl();
      this.tabViewData = new System.Windows.Forms.TabPage();
      this.btnLoadData = new System.Windows.Forms.Button();
      this.grdMainDataGrid = new System.Windows.Forms.DataGridView();
      this.tabEditData = new System.Windows.Forms.TabPage();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.txtBio = new System.Windows.Forms.TextBox();
      this.txtWebpage = new System.Windows.Forms.TextBox();
      this.txtEMail = new System.Windows.Forms.TextBox();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.txtGid = new System.Windows.Forms.Label();
      this.btnDelete = new System.Windows.Forms.Button();
      this.btnUpdate = new System.Windows.Forms.Button();
      this.btnInsert = new System.Windows.Forms.Button();
      this.btnCreateNew = new System.Windows.Forms.Button();
      this.btnNext = new System.Windows.Forms.Button();
      this.btnPrev = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.lblMessages = new System.Windows.Forms.Label();
      this.btnCancel = new System.Windows.Forms.Button();
      this.tabDataViews.SuspendLayout();
      this.tabViewData.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdMainDataGrid)).BeginInit();
      this.tabEditData.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabDataViews
      // 
      this.tabDataViews.Controls.Add(this.tabViewData);
      this.tabDataViews.Controls.Add(this.tabEditData);
      this.tabDataViews.Location = new System.Drawing.Point(12, 12);
      this.tabDataViews.Name = "tabDataViews";
      this.tabDataViews.SelectedIndex = 0;
      this.tabDataViews.Size = new System.Drawing.Size(618, 435);
      this.tabDataViews.TabIndex = 0;
      this.tabDataViews.SelectedIndexChanged += new System.EventHandler(this.TabDataViewsSelectedIndexChanged);
      // 
      // tabViewData
      // 
      this.tabViewData.Controls.Add(this.btnLoadData);
      this.tabViewData.Controls.Add(this.grdMainDataGrid);
      this.tabViewData.Location = new System.Drawing.Point(4, 22);
      this.tabViewData.Name = "tabViewData";
      this.tabViewData.Padding = new System.Windows.Forms.Padding(3);
      this.tabViewData.Size = new System.Drawing.Size(610, 409);
      this.tabViewData.TabIndex = 0;
      this.tabViewData.Text = "View Data";
      this.tabViewData.UseVisualStyleBackColor = true;
      // 
      // btnLoadData
      // 
      this.btnLoadData.Location = new System.Drawing.Point(529, 380);
      this.btnLoadData.Name = "btnLoadData";
      this.btnLoadData.Size = new System.Drawing.Size(75, 23);
      this.btnLoadData.TabIndex = 1;
      this.btnLoadData.Text = "Load Data";
      this.btnLoadData.UseVisualStyleBackColor = true;
      this.btnLoadData.Click += new System.EventHandler(this.BtnLoadDataClick);
      // 
      // grdMainDataGrid
      // 
      this.grdMainDataGrid.AllowUserToAddRows = false;
      this.grdMainDataGrid.AllowUserToDeleteRows = false;
      this.grdMainDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdMainDataGrid.Location = new System.Drawing.Point(6, 6);
      this.grdMainDataGrid.Name = "grdMainDataGrid";
      this.grdMainDataGrid.ReadOnly = true;
      this.grdMainDataGrid.Size = new System.Drawing.Size(598, 368);
      this.grdMainDataGrid.TabIndex = 0;
      // 
      // tabEditData
      // 
      this.tabEditData.BackColor = System.Drawing.SystemColors.Control;
      this.tabEditData.Controls.Add(this.tableLayoutPanel1);
      this.tabEditData.Controls.Add(this.btnDelete);
      this.tabEditData.Controls.Add(this.btnUpdate);
      this.tabEditData.Controls.Add(this.btnInsert);
      this.tabEditData.Controls.Add(this.btnCreateNew);
      this.tabEditData.Controls.Add(this.btnNext);
      this.tabEditData.Controls.Add(this.btnPrev);
      this.tabEditData.Location = new System.Drawing.Point(4, 22);
      this.tabEditData.Name = "tabEditData";
      this.tabEditData.Padding = new System.Windows.Forms.Padding(3);
      this.tabEditData.Size = new System.Drawing.Size(610, 409);
      this.tabEditData.TabIndex = 1;
      this.tabEditData.Text = "Edit Data";
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
      this.tableLayoutPanel1.Controls.Add(this.txtBio, 1, 4);
      this.tableLayoutPanel1.Controls.Add(this.txtWebpage, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.txtEMail, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.txtName, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.txtGid, 1, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 7);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 5;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(597, 367);
      this.tableLayoutPanel1.TabIndex = 6;
      // 
      // txtBio
      // 
      this.txtBio.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtBio.Location = new System.Drawing.Point(92, 123);
      this.txtBio.Multiline = true;
      this.txtBio.Name = "txtBio";
      this.txtBio.Size = new System.Drawing.Size(502, 241);
      this.txtBio.TabIndex = 7;
      // 
      // txtWebpage
      // 
      this.txtWebpage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtWebpage.Location = new System.Drawing.Point(92, 93);
      this.txtWebpage.Name = "txtWebpage";
      this.txtWebpage.Size = new System.Drawing.Size(502, 20);
      this.txtWebpage.TabIndex = 6;
      // 
      // txtEMail
      // 
      this.txtEMail.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtEMail.Location = new System.Drawing.Point(92, 63);
      this.txtEMail.Name = "txtEMail";
      this.txtEMail.Size = new System.Drawing.Size(502, 20);
      this.txtEMail.TabIndex = 5;
      // 
      // txtName
      // 
      this.txtName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtName.Location = new System.Drawing.Point(92, 33);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(502, 20);
      this.txtName.TabIndex = 4;
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label4.Location = new System.Drawing.Point(3, 120);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(83, 247);
      this.label4.TabIndex = 3;
      this.label4.Text = "Biography : ";
      this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label3.Location = new System.Drawing.Point(3, 90);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(83, 30);
      this.label3.TabIndex = 2;
      this.label3.Text = "Webpage : ";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label2.Location = new System.Drawing.Point(3, 60);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(83, 30);
      this.label2.TabIndex = 1;
      this.label2.Text = "EMail : ";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(3, 30);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(83, 30);
      this.label1.TabIndex = 0;
      this.label1.Text = "Name : ";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Location = new System.Drawing.Point(3, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(83, 30);
      this.label5.TabIndex = 8;
      this.label5.Text = "Gid : ";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtGid
      // 
      this.txtGid.AutoSize = true;
      this.txtGid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtGid.Location = new System.Drawing.Point(92, 0);
      this.txtGid.Name = "txtGid";
      this.txtGid.Size = new System.Drawing.Size(502, 30);
      this.txtGid.TabIndex = 9;
      this.txtGid.Text = "0";
      this.txtGid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btnDelete
      // 
      this.btnDelete.Location = new System.Drawing.Point(449, 380);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.Size = new System.Drawing.Size(75, 23);
      this.btnDelete.TabIndex = 5;
      this.btnDelete.Text = "Delete";
      this.btnDelete.UseVisualStyleBackColor = true;
      this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
      // 
      // btnUpdate
      // 
      this.btnUpdate.Location = new System.Drawing.Point(328, 380);
      this.btnUpdate.Name = "btnUpdate";
      this.btnUpdate.Size = new System.Drawing.Size(75, 23);
      this.btnUpdate.TabIndex = 4;
      this.btnUpdate.Text = "Update";
      this.btnUpdate.UseVisualStyleBackColor = true;
      this.btnUpdate.Click += new System.EventHandler(this.BtnUpdateClick);
      // 
      // btnInsert
      // 
      this.btnInsert.Enabled = false;
      this.btnInsert.Location = new System.Drawing.Point(207, 380);
      this.btnInsert.Name = "btnInsert";
      this.btnInsert.Size = new System.Drawing.Size(75, 23);
      this.btnInsert.TabIndex = 3;
      this.btnInsert.Text = "Insert";
      this.btnInsert.UseVisualStyleBackColor = true;
      this.btnInsert.Click += new System.EventHandler(this.BtnInsertClick);
      // 
      // btnCreateNew
      // 
      this.btnCreateNew.Location = new System.Drawing.Point(86, 380);
      this.btnCreateNew.Name = "btnCreateNew";
      this.btnCreateNew.Size = new System.Drawing.Size(75, 23);
      this.btnCreateNew.TabIndex = 2;
      this.btnCreateNew.Text = "Create New";
      this.btnCreateNew.UseVisualStyleBackColor = true;
      this.btnCreateNew.Click += new System.EventHandler(this.BtnCreateNewClick);
      // 
      // btnNext
      // 
      this.btnNext.Location = new System.Drawing.Point(570, 380);
      this.btnNext.Name = "btnNext";
      this.btnNext.Size = new System.Drawing.Size(34, 23);
      this.btnNext.TabIndex = 1;
      this.btnNext.Text = ">>";
      this.btnNext.UseVisualStyleBackColor = true;
      this.btnNext.Click += new System.EventHandler(this.BtnNextClick);
      // 
      // btnPrev
      // 
      this.btnPrev.Location = new System.Drawing.Point(6, 380);
      this.btnPrev.Name = "btnPrev";
      this.btnPrev.Size = new System.Drawing.Size(34, 23);
      this.btnPrev.TabIndex = 0;
      this.btnPrev.Text = "<<";
      this.btnPrev.UseVisualStyleBackColor = true;
      this.btnPrev.Click += new System.EventHandler(this.BtnPrevClick);
      // 
      // btnExit
      // 
      this.btnExit.Location = new System.Drawing.Point(551, 460);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 23);
      this.btnExit.TabIndex = 1;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.BtnExitClick);
      // 
      // lblMessages
      // 
      this.lblMessages.AutoSize = true;
      this.lblMessages.Location = new System.Drawing.Point(12, 469);
      this.lblMessages.Name = "lblMessages";
      this.lblMessages.Size = new System.Drawing.Size(0, 13);
      this.lblMessages.TabIndex = 2;
      // 
      // btnCancel
      // 
      this.btnCancel.Location = new System.Drawing.Point(470, 460);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Visible = false;
      this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
      // 
      // FrmMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(642, 495);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.lblMessages);
      this.Controls.Add(this.btnExit);
      this.Controls.Add(this.tabDataViews);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.Name = "FrmMainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Simple Postgres Example";
      this.tabDataViews.ResumeLayout(false);
      this.tabViewData.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grdMainDataGrid)).EndInit();
      this.tabEditData.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TabControl tabDataViews;
    private System.Windows.Forms.TabPage tabViewData;
    private System.Windows.Forms.TabPage tabEditData;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.Button btnLoadData;
    private System.Windows.Forms.DataGridView grdMainDataGrid;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtName;
    private System.Windows.Forms.TextBox txtEMail;
    private System.Windows.Forms.TextBox txtWebpage;
    private System.Windows.Forms.Button btnDelete;
    private System.Windows.Forms.Button btnUpdate;
    private System.Windows.Forms.Button btnInsert;
    private System.Windows.Forms.Button btnCreateNew;
    private System.Windows.Forms.Button btnNext;
    private System.Windows.Forms.Button btnPrev;
    private System.Windows.Forms.TextBox txtBio;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label txtGid;
    private System.Windows.Forms.Label lblMessages;
    private System.Windows.Forms.Button btnCancel;
  }
}

