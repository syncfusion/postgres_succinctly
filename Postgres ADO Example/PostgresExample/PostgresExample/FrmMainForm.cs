﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using PostgresExample.Classes;
using PostgresExample.Entities;

namespace PostgresExample
{
  public partial class FrmMainForm : Form
  {
    private List<MyTable> _loadedData;
    private int _editIndex;

    public FrmMainForm()
    {
      InitializeComponent();
    }

    private void BtnExitClick(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void BtnLoadDataClick(object sender, EventArgs e)
    {
      LoadData();
      grdMainDataGrid.DataSource = _loadedData;

    }

    private void BtnNextClick(object sender, EventArgs e)
    {
      _editIndex++;
      if (_editIndex >= _loadedData.Count)
      {
        _editIndex = 0;
      }
      SwitchToRecord(_editIndex);

    }

    private void BtnPrevClick(object sender, EventArgs e)
    {
      _editIndex--;
      if (_editIndex < 0)
      {
        _editIndex = _loadedData.Count - 1;
      }
      SwitchToRecord(_editIndex);

    }

    private void BtnCreateNewClick(object sender, EventArgs e)
    {
      txtGid.Text = "0";
      txtName.Text = string.Empty;
      txtEMail.Text = string.Empty;
      txtWebpage.Text = string.Empty;
      txtBio.Text = string.Empty;

      SwitchButtonsToNewMode();
      btnCancel.Visible = true;

    }

    private void BtnCancelClick(object sender, EventArgs e)
    {
      SwitchButtonsToRegularMode();
      SwitchToRecord(_editIndex);
      btnCancel.Visible = false;
    }

    private void BtnInsertClick(object sender, EventArgs e)
    {
      MyTable newMyTable = new MyTable
      {
        Gid = 0,
        Name = txtName.Text,
        Email = txtEMail.Text,
        Webpage = txtWebpage.Text,
        Bio = txtBio.Text
      };
      Database myDataBase = new Database();
      myDataBase.AddNew(newMyTable);
      _loadedData = myDataBase.GetAll();
      InitialiseEditTab();
      SwitchButtonsToRegularMode();
      btnCancel.Visible = false;
    }

    private void LoadData()
    {
      Database myDatabase = new Database();
      _loadedData = myDatabase.GetAll();
    }

    private void TabDataViewsSelectedIndexChanged(object sender, EventArgs e)
    {
      if(tabDataViews.SelectedIndex == 1) // If we switch to edit tab
      {
        if (_loadedData != null)
        {
          InitialiseEditTab();
        }
        else
        {
          MessageBox.Show("Data not loaded, plese go back to the 'View Data' tab and load the data");
        }
      }
    }

    private void InitialiseEditTab()
    {
      _editIndex = 0;

      txtGid.Text = _loadedData[_editIndex].Gid.ToString(CultureInfo.InvariantCulture);
      txtName.Text = _loadedData[_editIndex].Name;
      txtEMail.Text = _loadedData[_editIndex].Email;
      txtWebpage.Text = _loadedData[_editIndex].Webpage;
      txtBio.Text = _loadedData[_editIndex].Bio;

    }

    private void SwitchToRecord(int index)
    {
      txtGid.Text = _loadedData[index].Gid.ToString(CultureInfo.InvariantCulture);
      txtName.Text = _loadedData[index].Name;
      txtEMail.Text = _loadedData[index].Email;
      txtWebpage.Text = _loadedData[index].Webpage;
      txtBio.Text = _loadedData[index].Bio;
      
    }

    private void SwitchButtonsToNewMode()
    {
      btnInsert.Enabled = true;
      btnNext.Enabled = false;
      btnPrev.Enabled = false;
      btnUpdate.Enabled = false;
      btnDelete.Enabled = false;
      btnCreateNew.Enabled = false;

      lblMessages.Text = "New Record Mode Active, click Cancel to Abort.";

    }

    private void SwitchButtonsToRegularMode()
    {
      btnInsert.Enabled = false;
      btnNext.Enabled = true;
      btnPrev.Enabled = true;
      btnUpdate.Enabled = true;
      btnDelete.Enabled = true;
      btnCreateNew.Enabled = true;

      lblMessages.Text = string.Empty;
    }

    private void BtnUpdateClick(object sender, EventArgs e)
    {
      MyTable newMyTable = new MyTable
      {
        Gid = int.Parse(txtGid.Text),
        Name = txtName.Text,
        Email = txtEMail.Text,
        Webpage = txtWebpage.Text,
        Bio = txtBio.Text
      };
      Database myDataBase = new Database();
      myDataBase.Update(newMyTable);
      MessageBox.Show("Updated...");
      _loadedData = myDataBase.GetAll();
    }

    private void BtnDeleteClick(object sender, EventArgs e)
    {
      Database myDataBase = new Database();
      myDataBase.DeleteByGid(int.Parse(txtGid.Text));
      MessageBox.Show("Deleted...");
      _loadedData = myDataBase.GetAll();
      InitialiseEditTab();

    }

  }
}
