﻿namespace PostgresExample.Entities
{
  public class MyTable
  {
    public int Gid { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Webpage { get; set; }
    public string Bio { get; set; }

  }
}
