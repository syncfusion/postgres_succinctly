﻿using System;
using System.Configuration;

namespace PostgresExample.Classes
{
  public static class ConnectionStrings
  {
    public static string PgConnection
    {
      get
      {
        const string connectionStringName = "pgconnection";
        if (string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString))
        {
          throw new Exception("Connection string (" + connectionStringName + ") not defined in app.config!");
        }

        return ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
      }
    }

  }
}
