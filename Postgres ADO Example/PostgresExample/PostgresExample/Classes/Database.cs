﻿using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;
using PostgresExample.Entities;

namespace PostgresExample.Classes
{
  public class Database
  {
    public List<MyTable> GetAll()
    {
      List<MyTable> results = new List<MyTable>();

      using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionStrings.PgConnection))
      {
        connection.Open();
        const string sql = "select * from mytable";
        using (NpgsqlCommand command = new NpgsqlCommand(sql, connection))
        {
          using (NpgsqlDataReader reader = command.ExecuteReader())
          {
            if (!reader.HasRows) return results;
            while (reader.Read())
            {
              results.Add(new MyTable
                            {
                              Gid = (int)reader["gid"],
                              Name = (string)reader["name"],
                              Email = (string)reader["email"],
                              Webpage = (string)reader["webpage"],
                              Bio = (string)reader["bio"]
                            });
            }
          }
        }
      }

      return results;
    }

    public MyTable GetByGid(int gid)
    {
      MyTable result;

      using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionStrings.PgConnection))
      {
        connection.Open();
        const string sql = "select * from mytable where gid = :gid";
        using (NpgsqlCommand command = new NpgsqlCommand(sql, connection))
        {
          command.Parameters.Add(new NpgsqlParameter("gid", NpgsqlDbType.Integer));
          command.Parameters["gid"].Value = gid;

          using (NpgsqlDataReader reader = command.ExecuteReader())
          {
            if (!reader.HasRows) return null;
            reader.Read();
            result = new MyTable
            {
              Gid = (int)reader["gid"],
              Name = (string)reader["name"],
              Email = (string)reader["email"],
              Webpage = (string)reader["webpage"],
              Bio = (string)reader["bio"]
            };
          }
        }
      }

      return result;
    }

    public void AddNew(MyTable entity)
    {
      using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionStrings.PgConnection))
      {
        connection.Open();
        const string sql = "insert into mytable(name, email, webpage, bio) values(:name,:email,:webpage,:bio)";
        using(NpgsqlCommand command = new NpgsqlCommand(sql, connection))
        {
          command.Parameters.Add(new NpgsqlParameter("name", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("email", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("webpage", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("bio", NpgsqlDbType.Varchar));
          command.Parameters["name"].Value = entity.Name;
          command.Parameters["email"].Value = entity.Email;
          command.Parameters["webpage"].Value = entity.Webpage;
          command.Parameters["bio"].Value = entity.Bio;

          command.ExecuteNonQuery();
        }
      }
    }

    public void DeleteByGid(int gid)
    {
      using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionStrings.PgConnection))
      {
        connection.Open();
        const string sql = "delete from mytable where gid = :gid";
        using (NpgsqlCommand command = new NpgsqlCommand(sql, connection))
        {
          command.Parameters.Add(new NpgsqlParameter("gid", NpgsqlDbType.Integer));
          command.Parameters["gid"].Value = gid;

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(MyTable entity)
    {
      using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionStrings.PgConnection))
      {
        connection.Open();
        const string sql = "update mytable set name = :name, email = :email, webpage = :webpage, bio = :bio WHERE gid = :gid";
        using (NpgsqlCommand command = new NpgsqlCommand(sql, connection))
        {
          command.Parameters.Add(new NpgsqlParameter("name", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("email", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("webpage", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("bio", NpgsqlDbType.Varchar));
          command.Parameters.Add(new NpgsqlParameter("gid", NpgsqlDbType.Integer));
          command.Parameters["name"].Value = entity.Name;
          command.Parameters["email"].Value = entity.Email;
          command.Parameters["webpage"].Value = entity.Webpage;
          command.Parameters["bio"].Value = entity.Bio;
          command.Parameters["gid"].Value = entity.Gid;

          command.ExecuteNonQuery();
        }
      }

    }

  }
}
